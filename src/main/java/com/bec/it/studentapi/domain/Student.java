package com.bec.it.studentapi.domain;
// gqPAFM23ZfdyvHfTk78Q
public class Student {
    private String regno;
    private String fname;
    private String lname;

    public Student(String regno, String fname, String lname) {
        this.regno = regno;
        this.fname = fname;
        this.lname = lname;
    }

    public Student() {

    }
    public String getRegno() {
        return regno;
    }

    public void setRegno(String regno) {
        this.regno = regno;
    }

    public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public String getLname() {
        return lname;
    }

    public void setLname(String lname) {
        this.lname = lname;
    }
}
