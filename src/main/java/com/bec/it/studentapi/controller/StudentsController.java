package com.bec.it.studentapi.controller;

import com.bec.it.studentapi.domain.Student;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping( value="test/students")
public class StudentsController {
List<Student> students;
public StudentsController()
{
    students = new ArrayList();
    Student st1 = new Student("Y18AIT401", "Siva", "N");
    students.add(st1);
    Student st2 = new Student("Y18AIT402", "Ratna", "P");
    students.add(st2);

    Student st3 = new Student("Y18AIT403", "Ravi", "K");
    students.add(st3);
    Student st4 = new Student("Y18AIT404", "Srinivas", "N");
    students.add(st4);

}
   @GetMapping(value="/{id}")
public Student getStudent(@PathVariable String id){
    Student stu = students.stream()
            .filter(cs -> cs.getRegno().equalsIgnoreCase(id))
            .findFirst().get();

        return stu;
    }

    @PostMapping
    public String saveStudent(@RequestBody Student st)
    {
        students.add(st);
        return("Successful in adding student");

    }


@GetMapping
    public List<Student> getStudent1()
    {
        return students;
    }
}
